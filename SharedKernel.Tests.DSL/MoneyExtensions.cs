﻿using SharedKernel.Domain;

namespace SharedKernel.Tests.DSL {
    public static class MoneyExtensions {
        public static Money Rub(this int amount) {
            return new Money(amount, Money.Currency.Rub);
        }
    }
}