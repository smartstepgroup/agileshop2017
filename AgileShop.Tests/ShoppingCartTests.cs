using AgileShop.Domain;
using Xunit;

namespace AgileShop.Tests
{
    public class ShoppingCartTests
    {
        private readonly ProductFactory productFactory = new ProductFactory();

        [Fact]
        public void CanBuyProductsWithDiffentSizeAndColor()
        {
            var kanbanHookah = productFactory.CreateSimpleProduct("Канбан кальян", 100.Usd());
            Product kentBeckTears = productFactory.CreateSizedProduct("Слезы Кента Бека")
                .WithSize(125.ml(), 12.99.Usd())
                .WithSize(0.5.l(), 34.99.Usd())
                .WithSize(1.5.l(), 99.99.Usd());
            Product agileBeads = productFactory.CreateColorfulProduct("Аджальные шарики")
                .With(Color.Red, 15.99.Usd())
                .With(Color.Green, 15.99.Usd());

            var shoppingCart =
                new ShoppingCart()
                    .Add(kanbanHookah)
                    .Add(kentBeckTears.OfSize(125.ml()))
                    .Add(agileBeads.OfColor(Color.Red))
                    .IncrementQuantityOf(kentBeckTears.OfSize(125.ml()))
                    .IncrementQuantityOf(kentBeckTears.OfSize(125.ml()));

            Assert.Equal(1 + 3 + 1, shoppingCart.TotalQuantity);
            Assert.Equal(100m + 12.99m * 3 + 15.99m, shoppingCart.TotalCost);
            Assert.Equal(0, shoppingCart.TotalSavings);
        }
    }
}