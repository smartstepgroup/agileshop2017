﻿using SharedKernel.Domain;

namespace Storefront.Domain {
    public class Variation : ValueObject<Variation> {
        public Variation(string title, Money price) {
            Title = title;
            Price = price;
        }

        public string Title { get; }
        public Money Price { get; }
    }
}