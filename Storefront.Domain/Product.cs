﻿using System;
using System.Collections.Generic;
using SharedKernel.Domain;

namespace Storefront.Domain {
    public class Product : Entity<Guid> {
        public Product(string imageUrl, IEnumerable<Variation> variations) : base(Guid.NewGuid()) {
            ImageUrl = imageUrl;
            Variations = new List<Variation>(variations);
        }

        public string ImageUrl { get; }
        public ICollection<Variation> Variations { get; }
    }
}