using System.Collections.Generic;
using System.Linq;

namespace AgileShop.Domain
{
    public class ShoppingCart
    {
        private readonly IList<ShoppingCartItem> items = new List<ShoppingCartItem>();
        public IEnumerable<ShoppingCartItem> Items => items;

        public decimal TotalCost
        {
            get { return Items.Sum(_ => _.Cost); }
        }

        public decimal TotalQuantity
        {
            get { return Items.Sum(_ => _.Quantity); }
        }

        public decimal TotalSavings
        {
            get { return Items.Sum(_ => _.SavingPerItem * _.Quantity); }
        }

        public ShoppingCart Add(Product product)
        {
            items.Add(new ShoppingCartItem(product.DefaultVariation));
            return this;
        }

        public ShoppingCart Add(ProductVariation productVariation)
        {
            items.Add(new ShoppingCartItem(productVariation));
            return this;
        }

        public ShoppingCart IncrementQuantityOf(ProductVariation productVariation)
        {
            items.Single(_ => _.ProductVariation == productVariation).Quantity.Increase();
            return this;
        }
    }
}