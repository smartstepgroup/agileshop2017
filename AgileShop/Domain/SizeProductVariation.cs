namespace AgileShop.Domain
{
    public class SizeProductVariation : ProductVariation
    {
        public SizeProductVariation(Size size, Money price) :
            base(string.Format("{0} {1}", size.Value, size.UnitOfMeasure), price)
        {
            Size = size;
        }

        public Size Size { get; }
    }
}