using System.Collections.Generic;

namespace AgileShop.Domain
{
    public class ProductFactory
    {
        public Product CreateSimpleProduct(string name, Money price)
        {
            return new Product(name, price);
        }

        public ColorfulProductFactory CreateColorfulProduct(string name)
        {
            return new ColorfulProductFactory(name);
        }

        public SizedProductFactory CreateSizedProduct(string name)
        {
            return new SizedProductFactory(name);
        }
    }

    public class ColorfulProductFactory
    {
        private readonly string name;
        private readonly List<ColorProductVariation> productVariations = new List<ColorProductVariation>();

        public ColorfulProductFactory(string name)
        {
            this.name = name;
        }

        public ColorfulProductFactory With(Color color, Money price)
        {
            productVariations.Add(new ColorProductVariation(color, price));
            return this;
        }

        public static implicit operator Product(ColorfulProductFactory factory)
        {
            return new Product(factory.name, factory.productVariations);
        }
    }

    public class SizedProductFactory
    {
        private readonly string name;
        private readonly List<SizeProductVariation> productVariations = new List<SizeProductVariation>();

        public SizedProductFactory(string name)
        {
            this.name = name;
        }

        public SizedProductFactory WithSize(Size size, Money price)
        {
            productVariations.Add(new SizeProductVariation(size, price));
            return this;
        }

        public static implicit operator Product(SizedProductFactory factory)
        {
            return new Product(factory.name, factory.productVariations);
        }
    }
}