namespace AgileShop.Domain
{
    public enum UnitOfMeasure
    {
        Piece,
        Milliliter,
        Gram
    }
}