using System.Collections.Generic;
using System.Linq;

namespace AgileShop.Domain
{
    public class Product
    {
        internal Product(string name, Money price)
        {
            Name = name;
            Variations = new List<ProductVariation>
            {
                new ProductVariation(Name, price)
            };
        }

        internal Product(string name, IEnumerable<ProductVariation> productVariations)
        {
            Name = name;
            Variations = new List<ProductVariation>(productVariations);
        }

        public string Name { get; }
        public IList<ProductVariation> Variations { get; }
        public ProductVariation DefaultVariation => Variations.First();

        public ProductVariation OfSize(Size size)
        {
            return Variations.OfType<SizeProductVariation>().Single(_ => _.Size == size);
        }

        public ProductVariation OfColor(Color color)
        {
            return Variations.OfType<ColorProductVariation>().Single(_ => _.Color == color);
        }
    }
}