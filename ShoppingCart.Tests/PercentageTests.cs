﻿#region Usings

using NUnit.Framework;
using ShoppingCart.Domain;

#endregion

namespace ShoppingCart.Tests {
    [TestFixture]
    public class PercentageTests {
        [Test]
        public void CanCreatePercentageWithAllowedValues() {
            Assert.DoesNotThrow(() => new Percentage(0));
            Assert.DoesNotThrow(() => new Percentage(1));
            Assert.DoesNotThrow(() => new Percentage(99));
            Assert.DoesNotThrow(() => new Percentage(100));
        }
    }
}