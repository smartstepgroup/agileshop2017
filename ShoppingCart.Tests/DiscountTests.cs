using NUnit.Framework;
using SharedKernel.Tests.DSL;
using ShoppingCart.Domain;
using ShoppingCart.Tests.DSL;

namespace ShoppingCart.Tests {
    [TestFixture]
    public class DiscountTests {
        [Test]
        public void CanCalculateDiscountAmount() {
            var discount = new Discount(1000.Rub(), 5.Percent());

            Assert.AreEqual(50.Rub(), discount.Amount);
        }

        [Test]
        public void CanCalculateDiscountedPrice() {
            var discount = new Discount(1000.Rub(), 5.Percent());

            Assert.AreEqual(950.Rub(), discount.DiscountedPrice);
        }

        [Test]
        public void ZeroDiscountDoesnNotChangeAmount() {
            var discount = new Discount(1000.Rub(), 0.Percent());

            Assert.AreEqual(1000.Rub(), discount.DiscountedPrice);
        }
    }
}