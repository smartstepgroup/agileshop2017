﻿using ShoppingCart.Domain;

namespace ShoppingCart.Tests.DSL {
    public static class PercentExtensions {
        public static Percentage Percent(this int amount) {
            return new Percentage(amount);
        }
    }
}