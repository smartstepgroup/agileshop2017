﻿#region Usings

using Moq;

#region Usings

using ShoppingCart.Domain;

#endregion

#endregion

namespace ShoppingCart.Tests.DSL {
    internal class PromoActionFactory {
        private string promoCode;
        private Percentage percentageDiscount;

        public PromoActionFactory WithPromoCode(string promoCode) {
            this.promoCode = promoCode;
            return this;
        }

        public PromoActionFactory WithPercentageDiscount(Percentage percentageDiscount) {
            this.percentageDiscount = percentageDiscount;
            return this;
        }

        public static implicit operator PromoAction(PromoActionFactory factory) {
            return new PromoAction(factory.promoCode, factory.percentageDiscount);
        }

        internal PromoAction Persist(Mock<IPromoActionRepository> promoRepository) {
            var promoAction = (PromoAction) this;
            promoRepository.Setup(_ => _.Get(promoCode)).Returns(promoAction);
            return promoAction;
        }
    }
}