﻿namespace ShoppingCart.Tests.DSL {
    internal class Factory {
        public ProductFactory Product => new ProductFactory();

        public CartFactory Cart => new CartFactory();

        public PromoActionFactory PromoAction => new PromoActionFactory();
    }
}