﻿using System;
using SharedKernel.Domain;
using ShoppingCart.Domain;

namespace ShoppingCart.Tests.DSL {
    internal class ProductFactory {
        private Money price = Money.Zero;
        private string title = string.Empty;

        public ProductFactory WithPrice(Money price) {
            this.price = price;
            return this;
        }

        public ProductFactory WithTitle(string title) {
            this.title = title;
            return this;
        }

        public static implicit operator Product(ProductFactory factory) {
            return new Product(Guid.NewGuid(), factory.title, factory.price, string.Empty);
        }
    }
}