﻿using System;
using ShoppingCart.Domain;

namespace ShoppingCart.Tests.DSL {
    public static class QuantityExtensions {
        public static Quantity Pieces(this int amount) {
            return new Quantity(Convert.ToUInt32(amount));
        }
    }
}