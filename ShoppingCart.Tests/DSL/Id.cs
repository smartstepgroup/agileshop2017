﻿namespace ShoppingCart.Tests.DSL {
    internal static class Id {
        private static int id = 1;
        internal static int Next => id++;
    }
}