﻿using ShoppingCart.Domain;

namespace ShoppingCart.Tests.DSL {
    internal class CartFactory {
        private readonly Cart cart = new Cart();

        public CartFactory With(params Product[] products) {
            foreach (var product in products) {
                cart.Buy(product);
            }
            return this;
        }

        public static implicit operator Cart(CartFactory factory) {
            return factory.cart;
        }
    }
}