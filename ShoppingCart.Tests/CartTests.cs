﻿using System.Linq;
using NUnit.Framework;
using SharedKernel.Tests.DSL;
using ShoppingCart.Domain;
using ShoppingCart.Tests.DSL;

namespace ShoppingCart.Tests {
    [TestFixture]
    internal class CartTests : Test {
        [Test]
        public void BuyProduct_AddsCartItemWithSameProduct() {
            Cart cart = a.Cart;
            Product product = a.Product;

            cart.Buy(product);

            CollectionAssert.AreEqual(new [] {product.Id}, cart.Products.Select(_ => _.Id));
        }

        [Test]
        public void BuyProduct_CalculatesCartTotalAmount() {
            Cart cart = a.Cart;

            cart.Buy(a.Product.WithPrice(10.Rub()));

            Assert.That(cart.TotalAmount, Is.EqualTo(10.Rub()));
        }

        [Test]
        public void BuyTheSameProduct_IncreasesQuantity() {
            Cart cart = a.Cart;
            Product product = a.Product.WithPrice(10.Rub());

            cart.Buy(product);
            cart.Buy(product);

            Assert.That(cart.Products.Single().Quantity, Is.EqualTo(2.Pieces()));
        }

        [Test]
        public void BuyProduct_NoTotalSaving() {
            Cart cart = a.Cart;

            cart.Buy(a.Product.WithPrice(10.Rub()));

            Assert.That(cart.TotalSaving, Is.EqualTo(0.Rub()));
        }

        [Test]
        public void BuySecondProduct_IncreasesCartTotalAmount() {
            Cart cart = a.Cart;

            cart.Buy(a.Product.WithPrice(10.Rub()));
            cart.Buy(a.Product.WithPrice(15.Rub()));

            Assert.That(cart.TotalAmount, Is.EqualTo((10 + 15).Rub()));
        }

        [Test]
        public void Apply20PercentDiscount_ReducesTotalAmountBy20Percent() {
            Product product = a.Product.WithPrice(10.Rub());
            Cart cart = a.Cart.With(product);

            cart.ApplyDiscount(product, 20.Percent());

            Assert.That(cart.TotalAmount, Is.EqualTo(8.Rub()));
        }

        [Test]
        public void Apply20PercentDiscount_CalculatesTotalSaving() {
            Product product = a.Product.WithPrice(10.Rub());
            Cart cart = a.Cart.With(product);

            cart.ApplyDiscount(product, 20.Percent());

            Assert.That(cart.TotalSaving, Is.EqualTo(2.Rub()));
        }

        [Test]
        public void Delete_RemovesProductFromCart() {
            Product scrumAmulet = a.Product.WithTitle("Scrum Amulet");
            Product agileBalls = a.Product.WithTitle("Agile Balls");
            Cart cart = a.Cart.With(scrumAmulet, agileBalls);

            cart.Delete(agileBalls);

            CollectionAssert.AreEqual(new [] {"Scrum Amulet"}, cart.Products.Select(_ => _.Title));
        }

        [Test]
        public void Delete_UpdatesTotalAmount() {
            Product scrumAmulet = a.Product
                .WithTitle("Scrum Amulet")
                .WithPrice(1.Rub());
            Product agileBalls = a.Product
                .WithTitle("Agile Balls")
                .WithPrice(2.Rub());
            Cart cart = a.Cart.With(scrumAmulet, agileBalls);

            cart.Delete(agileBalls);

            Assert.That(cart.TotalAmount, Is.EqualTo(1.Rub()));
        }

        [Test]
        public void IncreaseQuantity_IncreasesTotalAmount() {
            Product product = a.Product.WithPrice(10.Rub());
            Cart cart = a.Cart.With(product);

            cart.IncreaseQuantityOf(product);

            Assert.That(cart.TotalAmount, Is.EqualTo((2 * 10).Rub()));
        }

        [Test]
        public void DecreaseQuantity_ReducesTotalAmount() {
            Product product = a.Product.WithPrice(10.Rub());
            Cart cart = a.Cart.With(product, product);

            cart.DecreaseQuantityOf(product);

            Assert.That(cart.TotalAmount, Is.EqualTo((1 * 10).Rub()));
        }

        [Test]
        public void DecreaseQuantityOfLastItem_ClearsTotalAmount() {
            Product product = a.Product.WithPrice(10.Rub());
            Cart cart = a.Cart.With(product);

            cart.DecreaseQuantityOf(product);

            Assert.That(cart.TotalAmount, Is.EqualTo(0.Rub()));
        }

        [Test]
        public void DecreaseQuantityOfLastItem_RemovesProductFromCart() {
            Product product = a.Product.WithPrice(10.Rub());
            Cart cart = a.Cart.With(product);

            cart.DecreaseQuantityOf(product);

            Assert.That(cart.Products, Is.Empty);
        }
    }
}