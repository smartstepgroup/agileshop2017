﻿#region Usings

using Moq;
using NUnit.Framework;
using SharedKernel.Tests.DSL;
using ShoppingCart.Domain;
using ShoppingCart.Tests.DSL;

#endregion

namespace ShoppingCart.Tests {
    [TestFixture]
    internal class PromoCodeServiceTests : Test {
        [Test]
        public void CanApplyPromoCodeToCart() {
            var promoRepository = new Mock<IPromoActionRepository>();
            PromoAction promoAction = a.PromoAction
                                       .WithPromoCode("SuperDeal2017")
                                       .WithPercentageDiscount(20.Percent())
                                       .Persist(promoRepository);
            Product product = a.Product.WithPrice(10.Rub());
            Cart cart = a.Cart.With(product);
            var promoCodeService = new PromoCodeService(promoRepository.Object);

            promoCodeService.Apply(cart, "SuperDeal2017");

            Assert.That(cart.TotalAmount, Is.EqualTo(8.Rub()));
        }

        [Test]
        public void NotExistingPromoCodeDoesNotWork() {
            var promoRepository = new Mock<IPromoActionRepository>();
            Product product = a.Product.WithPrice(10.Rub());
            Cart cart = a.Cart.With(product);
            var promoCodeService = new PromoCodeService(promoRepository.Object);

            promoCodeService.Apply(cart, "NotExistingPromoCode");

            Assert.That(cart.TotalAmount, Is.EqualTo(10.Rub()));
        }
    }
}