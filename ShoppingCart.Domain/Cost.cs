﻿using SharedKernel.Domain;

namespace ShoppingCart.Domain {
    public class Cost : ValueObject<Cost> {
        public Cost(Money price, Quantity quantity) : this(quantity, new Discount(price, new Percentage(0))) {}

        public Quantity Quantity { get; }
        public Discount Discount { get; }
        public Money Amount { get; }

        public Cost Create(Quantity quantity, Discount discount) {
            return new Cost(quantity, discount);
        }

        private Cost(Quantity quantity, Discount discount) {
            Quantity = quantity;
            Discount = discount;
            Amount = Discount.DiscountedPrice * quantity;
        }

        public Cost IncreaseQuantity() {
            return Create(Quantity.Increase(), Discount);
        }

        public Cost DecreaseQuantity() {
            return Create(Quantity.Decrease(), Discount);
        }

        public Cost ApplyDiscount(Discount discount) {
            return Create(Quantity, discount);
        }
    }
}