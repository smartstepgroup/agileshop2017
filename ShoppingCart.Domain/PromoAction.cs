﻿using SharedKernel.Domain;

namespace ShoppingCart.Domain {
    public class PromoAction : ValueObject<PromoAction> {
        public PromoAction(string promoCode, Percentage percentagePercentageDiscount) {
            PromoCode = promoCode;
            PercentageDiscount = percentagePercentageDiscount;
        }

        public string PromoCode { get; }
        public Percentage PercentageDiscount { get; }
    }
}