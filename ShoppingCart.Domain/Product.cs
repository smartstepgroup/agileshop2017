﻿using System;
using SharedKernel.Domain;

namespace ShoppingCart.Domain {
    public class Product : ValueObject<Product> {
        public Product(Guid id, string title, Money price, string imageUrl) {
            Id = id;
            Title = title;
            Price = price;
            ImageUrl = imageUrl;
        }

        public Guid Id { get; }
        public string Title { get; }
        public Money Price { get; }
        public string ImageUrl { get; }
    }
}