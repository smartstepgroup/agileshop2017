﻿using SharedKernel.Domain;

namespace ShoppingCart.Domain {
    public class Quantity : ValueObject<Quantity>
    {
        private readonly uint value;

        public Quantity(uint value)
        {
            this.value = value;
        }

        public Quantity Increase()
        {
            return new Quantity(value + 1);
        }

        public Quantity Decrease() {
            return value > 0
                ? new Quantity(value - 1)
                : new Quantity(0);
        }

        public static implicit operator uint(Quantity quantity)
        {
            return quantity.value;
        }
    }
}