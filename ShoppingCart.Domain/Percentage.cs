﻿using System.Diagnostics.Contracts;
using SharedKernel.Domain;

namespace ShoppingCart.Domain {
    public class Percentage : ValueObject<Percentage> {
        public Percentage(int value) {
            Contract.Assert(value >= 0, "Percentage should be in range [0, 100].");
            Contract.Assert(value <= 100, "Percentage should be in range [0, 100].");

            Value = value;
        }

        public int Value { get; }
    }
}