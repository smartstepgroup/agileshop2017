﻿namespace ShoppingCart.Domain {
    public class PromoCodeService {
        private readonly IPromoActionRepository promoActionRepository;

        public PromoCodeService(IPromoActionRepository promoActionRepository) {
            this.promoActionRepository = promoActionRepository;
        }

        public void Apply(Cart cart, string promocode) {
            var promoAction = promoActionRepository.Get(promocode);
            foreach (var cartItem in cart.Products) {
                if (IsApplicable(promoAction, cartItem)) Apply(promoAction, cartItem);
            }
        }

        private void Apply(PromoAction promoAction, CartProduct cartProduct) {
            var discount = new Discount(cartProduct.Price, promoAction.PercentageDiscount);
            cartProduct.ApplyDiscount(discount);
        }

        private bool IsApplicable(PromoAction promoAction, CartProduct cartProduct) {
            /*A lot of logic*/
            return promoAction != null;
        }
    }
}