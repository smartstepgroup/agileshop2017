﻿using SharedKernel.Domain;

namespace ShoppingCart.Domain {
    public class Discount : ValueObject<Discount> {
        public Discount(Money originalPrice, Percentage percentage) {
            this.originalPrice = originalPrice;
            Percentage = percentage;
        }

        private readonly Money originalPrice;
        public Percentage Percentage { get; }
        public Money Amount => originalPrice * (Percentage.Value / 100.0m);
        public Money DiscountedPrice => originalPrice - Amount;
    }
}