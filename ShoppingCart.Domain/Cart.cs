﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Contracts;
using System.Linq;
using SharedKernel.Domain;

namespace ShoppingCart.Domain {
    public class Cart : Entity<Guid> {
        public Cart() {
            products = new List<CartProduct>();
        }

        private readonly List<CartProduct> products;
        public ReadOnlyCollection<CartProduct> Products => products.AsReadOnly();
        public Money TotalAmount => new Money(products.Sum(_ => _.Cost.Amount), Money.Currency.Rub);
        public Money TotalSaving => new Money(products.Sum(_ => _.Cost.Discount.Amount), Money.Currency.Rub);

        public void Buy(Product product) {
            if (ThereIsA(product)) IncreaseQuantityOf(product);
            else Add(product);
        }

        public void Delete(Product product) {
            Contract.Assert(ThereIsA(product),
                $"Unable to find product '{product.Title}' in the cart.");

            products.Remove(ItemOf(product));
        }

        public void ApplyDiscount(Product product, Percentage percent) {
            Contract.Assert(ThereIsA(product),
                $"Unable to find product '{product.Title}' in the cart.");

            products
                .Where(_ => _.Id == product.Id)
                .ToList()
                .ForEach(_ => _.ApplyDiscount(new Discount(_.Price, percent)));
        }

        public void IncreaseQuantityOf(Product product) {
            ItemOf(product).IncreaseQuantity();
        }

        public void DecreaseQuantityOf(Product product) {
            var cartItem = ItemOf(product);
            if (cartItem.Quantity == 1) Delete(product);
            else cartItem.DecreaseQuantity();
        }

        private void Add(Product product) {
            products.Add(new CartProduct(product));
        }

        private CartProduct ItemOf(Product product) {
            Contract.Assert(ThereIsA(product));

            return products.Single(_ => _.Id == product.Id);
        }

        private bool ThereIsA(Product product) {
            return products.Any(_ => _.Id == product.Id);
        }
    }
}