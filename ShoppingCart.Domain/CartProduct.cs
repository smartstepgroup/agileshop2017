﻿using System;
using SharedKernel.Domain;

namespace ShoppingCart.Domain {
    public class CartProduct : Entity<Guid> {
        internal CartProduct(Product product) : base(product.Id) {
            Title = product.Title;
            Price = product.Price;
            ImageUrl = product.ImageUrl;
            Cost = new Cost(product.Price, new Quantity(1));
        }

        public string Title { get; }
        public Money Price { get; }
        public string ImageUrl { get; }
        public Quantity Quantity => Cost.Quantity;
        public Cost Cost { get; private set; }

        public void ApplyDiscount(Discount discount) {
            Cost = Cost.ApplyDiscount(discount);
        }

        public void IncreaseQuantity() {
            Cost = Cost.IncreaseQuantity();
        }

        public void DecreaseQuantity() {
            Cost = Cost.DecreaseQuantity();
        }
    }
}