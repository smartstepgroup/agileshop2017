namespace ShoppingCart.Domain {
    public interface IPromoActionRepository {
        PromoAction Get(string promoCode);
    }
}