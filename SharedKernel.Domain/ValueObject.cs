﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace SharedKernel.Domain {
    public abstract class ValueObject<T> : IEquatable<T> where T : ValueObject<T> {

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            if (obj.GetType() != GetType()) return false;

            return Equals(obj as T);
        }

        public virtual bool Equals(T other) {
            if (other == null) return false;

            var t = GetType();
            var otherType = other.GetType();

            if (t != otherType) return false;

            var fields = t.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            foreach (var field in fields) {
                var value1 = field.GetValue(other);
                var value2 = field.GetValue(this);

                if (value1 == null) {
                    if (value2 != null) return false;
                }
                else if (!value1.Equals(value2))
                    return false;
            }

            return true;
        }

        private IEnumerable<FieldInfo> GetFields() {
            var t = GetType();
            var fields = new List<FieldInfo>();

            while (t != typeof(object)) {
                fields.AddRange(t.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public));
                t = t.BaseType;
            }

            return fields;
        }

        public override int GetHashCode() {
            var fields = GetFields();
            const int startValue = 17;
            const int multiplier = 59;

            var hashCode = startValue;
            foreach (var field in fields) {
                var value = field.GetValue(this);
                if (value != null)
                    hashCode = hashCode * multiplier + value.GetHashCode();
            }
            return hashCode;
        }

        public static bool operator ==(ValueObject<T> left, ValueObject<T> right) {
            return Equals(left, right);
        }

        public static bool operator !=(ValueObject<T> left, ValueObject<T> right) {
            return !(left == right);
        }

        public override string ToString() {
            var stringBuilder = new StringBuilder();
            foreach (var property in GetFields()) {
                var propertyValue = property.GetValue(this);
                if (null == propertyValue) continue;

                stringBuilder.Append(propertyValue);
            }

            return stringBuilder.ToString();
        }
    }
}