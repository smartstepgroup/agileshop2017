﻿using System;
using System.Diagnostics.Contracts;

namespace SharedKernel.Domain {
    public class Money : ValueObject<Money>
    {
        public Money(decimal amount, string currencyCode = "RUB")
        {
            Amount = Math.Round(amount, 2);
            CurrencyCode = currencyCode;
        }

        private decimal Amount { get; }
        private string CurrencyCode { get; }

        public static Money Zero => new Money(0, Currency.Rub);

        public static implicit operator decimal(Money money)
        {
            return money.Amount;
        }

        public static Money operator +(Money money, decimal d)
        {
            return new Money(money.Amount + d, money.CurrencyCode);
        }

        public static Money operator +(Money a, Money b)
        {
            Contract.Assert(a.CurrencyCode.Equals(b.CurrencyCode), "Multicurrency operations are not supported yet. Sorry :(");

            return new Money(a.Amount + b.Amount, a.CurrencyCode);
        }

        public static Money operator -(Money money, decimal d)
        {
            return new Money(money.Amount - d, money.CurrencyCode);
        }

        public static Money operator -(Money a, Money b)
        {
            Contract.Assert(a.CurrencyCode.Equals(b.CurrencyCode), "Multicurrency operations are not supported yet. Sorry :(");

            return new Money(a.Amount - b.Amount, a.CurrencyCode);
        }

        public static Money operator *(Money money, decimal d)
        {
            return new Money(money.Amount * d, money.CurrencyCode);
        }

        public static class Currency
        {
            public static string Rub = "RUB";
            public static string Usd = "USD";
        }
    }
}