﻿namespace SharedKernel.Domain {
    public class Entity<T> : IEntity<T> {
        public T Id { get; }

        public Entity(T id) {
            Id = id;
        }

        protected Entity() {}
    }
}