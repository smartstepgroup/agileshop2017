﻿using NUnit.Framework;
using SharedKernel.Tests.DSL;

namespace Storefront.Domain.Tests {
    [TestFixture]
    public class VariationTests {
        [Test]
        public void VariationIsValueObject() {
            Assert.AreEqual(new Variation("1 кг", 100.Rub()), new Variation("1 кг", 100.Rub()));
            Assert.True(new Variation("1 кг", 100.Rub()) == new Variation("1 кг", 100.Rub()));
        }
    }
}